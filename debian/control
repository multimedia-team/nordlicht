Source: nordlicht
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders: Peter Spiess-Knafl <dev@spiessknafl.at>
Section: video
Build-Depends: cmake,
               debhelper-compat (= 13),
               help2man,
               libavcodec-dev,
               libavformat-dev,
               libpopt-dev,
               libswscale-dev
Standards-Version: 4.5.1
Homepage: https://github.com/nordlicht/nordlicht
Vcs-Git: https://salsa.debian.org/multimedia-team/nordlicht.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/nordlicht

Package: nordlicht
Architecture: any
Depends: libnordlicht0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: create colorful video barcodes
 nordlicht converts video files into colorful barcodes. It's heavily inspired
 by the "moviebarcode" tumblr. It takes the video's frames in regular
 intervals, scales them to 1px width, and appends them. The resulting barcodes
 can be integrated into video players for simplified navigation.
 .
 This package contains the `nordlicht' tool which can be used to create
 barcodes from video files.

Package: libnordlicht0
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: create colorful video barcodes -- shared library
 nordlicht converts video files into colorful barcodes. It's heavily inspired
 by the "moviebarcode" tumblr. It takes the video's frames in regular
 intervals, scales them to 1px width, and appends them. The resulting barcodes
 can be integrated into video players for simplified navigation.
 .
 This package contains the library which encapsulates the video barcode
 functionality.

Package: libnordlicht-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libnordlicht0 (= ${binary:Version}), ${misc:Depends}
Description: create colorful video barcodes -- development files
 nordlicht converts video files into colorful barcodes. It's heavily inspired
 by the "moviebarcode" tumblr. It takes the video's frames in regular
 intervals, scales them to 1px width, and appends them. The resulting barcodes
 can be integrated into video players for simplified navigation.
 .
 This package contains the development files which are needed if you want to
 include nordlicht functionality into your own applications.
